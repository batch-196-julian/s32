let http = require("http");

let courses = [
	{

		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{

		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{

		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	},

]

http.createServer(function(request,response){

	console.log(request.url);
	console.log(request.method);

	if (request.url === "/" && request.method === "GET"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("This is the / endpoint. GET method request.");

	} else if (request.url === "/" && request.method === "POST"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("This is the / endpoint. POST method request.");

	} else if (request.url === "/" && request.method === "PUT"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("This is the / endpoint. PUT method request.");

	} else if (request.url === "/" && request.method === "DELETE"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("This is the / endpoint. DELETE method request.");

	}else if (request.url === "/courses" && request.method === "GET"){

		response.writeHead(200,{'Content-Type': 'application/json'});
		response.end(JSON.stringify(courses));

	}else if (request.url === "/courses" && request.method === "POST"){


		let requestBody = "";

		request.on('data',function(data){

			// console.log(data);
			requestBody += data;
		})

			request.on('end',function(){
				requestBody = JSON.parse(requestBody);
				// console.log(requestBody);
				courses.push(requestBody);
				// console.log(courses);

			response.writeHead(200,{'Content-Type': 'application/json'});
			response.end(JSON.stringify(courses));
			})

		
	}

}).listen(4000);

console.log("Server running at localhost:4000");